from django.test import TestCase
from mock import Mock
from testApp.middleware.custom_middleware import LogsMiddleware
import redis


def get_redis_connection():
    return redis.Redis(
        host="redis-18134.c283.us-east-1-4.ec2.cloud.redislabs.com",
        port="18134",
        db="0",
        password="wR8XBMk1q4ALATZZkWhxjisDo6ltmJ6Z",
        decode_responses=True
    )


class MiddlewareTestCase(TestCase):
    def setUp(self):
        self.middleware = LogsMiddleware(self)
        self.request = Mock()
        self.request.session = {}

    def test_middleware(self):
        redis_connection = get_redis_connection()
        redis_connection.set("user_ip", "192.168.1")
        value = redis_connection.get("user_ip")
        self.assertEqual(value, "192.168.1")


def test_middleware(self):
    self.assertIsNone(self.middleware.process_request(self.request))
    self.assertIsInstance(self.request.logsMiddleware, LogsMiddleware)
