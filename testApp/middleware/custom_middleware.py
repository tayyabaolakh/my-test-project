from genericpath import exists
from django.contrib.auth.models import Group
from django.utils import timezone
import redis
from django.core.exceptions import PermissionDenied


def get_redis_connection():
    return redis.Redis(
        host="redis-18134.c283.us-east-1-4.ec2.cloud.redislabs.com",
        port="18134",
        db="0",
        password="wR8XBMk1q4ALATZZkWhxjisDo6ltmJ6Z",
        decode_responses=True
    )


class LogsMiddleware(object):

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if not request.session.session_key:
            request.session.create()

        user_logs_data = dict()
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        user_logs_data["ip_address"] = x_forwarded_for.split(',')[0] if x_forwarded_for else request.META.get('REMOTE_ADDR')
        user_logs_data["timestamp"] = timezone.now()

        try:
            # Task 1
            pythonLogging(user_logs_data["ip_address"], user_logs_data["timestamp"])
            # Task 2
            redis_middleware(user_logs_data["ip_address"], request)
            # Task 3
            if request.user is exists:
                group = Group.objects.get(name=request.user.groups.all()[0])
                user_gorup_middleware(user_logs_data["ip_address"], group)
            else:
                group = "Gold"
                user_gorup_middleware(user_logs_data["ip_address"], group)

        except Exception as e:
            raise e

        response = self.get_response(request)
        return response


# Task1
def pythonLogging(user_ip, timestamp):
    f = open("logs.txt", "a")
    f.write("IP: "+user_ip+", Time: "+str(timestamp)+"\n")
    f.close()


# Task2
def redis_middleware(user_ip, request):
    try:
        redis_connection = get_redis_connection()
        total_calls = 0
        if redis_connection.get(user_ip) is not None:
            total_calls = redis_connection.get(user_ip)
            if int(total_calls) >= 5:
                raise PermissionDenied
            else:
                total_calls = int(total_calls)+1
                remaining_key_time = redis_connection.ttl(user_ip)
                redis_connection.set(user_ip, total_calls, ex=remaining_key_time)
        else:
            total_calls = int(total_calls) + 1

        redis_connection.set(user_ip, total_calls, ex=60)

    except Exception as e:
        raise e


# Task3
def user_gorup_middleware(user_ip, group):
    if group != '':
        if group == 'Gold':
            access_endpoints(user_ip, 10)
        elif group == 'Silver':
            access_endpoints(user_ip, 5)
        elif group == 'Bronze':
            access_endpoints(user_ip, 2)
    else:
        print("User undefined")


def access_endpoints(user_ip, time):
    try:
        redis_connection = get_redis_connection()
        total_calls = 0

        if redis_connection.get(user_ip) is not None:
            total_calls = redis_connection.get(user_ip)
            if int(total_calls) >= time:
                raise PermissionDenied
            else:
                total_calls = int(total_calls) + 1
                remaining_key_time = redis_connection.ttl(user_ip)
                redis_connection.set(user_ip, total_calls, ex=remaining_key_time)
        else:
            total_calls = int(total_calls) + 1

        redis_connection.set(user_ip, total_calls, ex=60)

    except Exception as e:
        raise e
